package ru.hayova.githubusers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabContentFactory;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements TabHost.OnTabChangeListener {

    static final int NUM_ITEMS = 3;

    public static final String regexPart1 = "^[a-hA-H].+$";
    public static final String regexPart2 = "^[i-pI-P].+$";
    public static final String regexPart3 = "^[q-zQ-Z].+$";

    private TabHost mTabHost;
    private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, MainActivity.TabInfo>();

    private FragmentStatePagerAdapter mAdapter;
    private ViewPager mPager;

    private SearchView mSearchView;

    private DatabaseHelper mDb;

    private static UserCursorAdapter mUserCursorAdapterA;
    private static UserCursorAdapter mUserCursorAdapterI;
    private static UserCursorAdapter mUserCursorAdapterQ;

    private class TabInfo {
        private String tag;
        private Class<?> clss;
        private Bundle args;
        private Fragment fragment;

        TabInfo(String tag, Class<?> clazz, Bundle args) {
            this.tag = tag;
            this.clss = clazz;
            this.args = args;
        }

    }

    class TabFactory implements TabContentFactory {

        private final Context mContext;

        public TabFactory(Context context) {
            mContext = context;
        }

        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }

    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);

        this.initialiseTabHost(savedInstanceState);

        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        new LoadGitHubUsersListTask().execute();
    }

    private void clearSearch(){
        mSearchView.setQuery("", false);
        mSearchView.setIconified(true);

        mUserCursorAdapterA.changeCursor(mDb.getUsersByName(regexPart1, null));
        mUserCursorAdapterI.changeCursor(mDb.getUsersByName(regexPart2, null));
        mUserCursorAdapterQ.changeCursor(mDb.getUsersByName(regexPart3, null));
    }

    private void findText(String query) {
        Integer page = mTabHost.getCurrentTab();

        if (page == 0)
            mUserCursorAdapterA.changeCursor(mDb.getUsersByName(regexPart1, query));
        else if (page == 1)
            mUserCursorAdapterI.changeCursor(mDb.getUsersByName(regexPart2, query));
        else if (page == 2)
            mUserCursorAdapterQ.changeCursor(mDb.getUsersByName(regexPart3, query));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchView.setQuery("", false);

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.search);

        mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                findText(query);

                return false;
            }
        });

        return true;
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("tab", mTabHost.getCurrentTabTag());
        super.onSaveInstanceState(outState);
    }

    private void initialiseTabHost(Bundle args) {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
        TabInfo tabInfo = null;
        MainActivity.AddTab(this, mTabHost, mTabHost.newTabSpec("Tab1").setIndicator("A-H"), (tabInfo = new TabInfo("Tab1", ArrayListFragment.class, args)));
        mapTabInfo.put(tabInfo.tag, tabInfo);
        MainActivity.AddTab(this, mTabHost, mTabHost.newTabSpec("Tab2").setIndicator("I-P"), (tabInfo = new TabInfo("Tab2", ArrayListFragment.class, args)));
        mapTabInfo.put(tabInfo.tag, tabInfo);
        MainActivity.AddTab(this, mTabHost, mTabHost.newTabSpec("Tab3").setIndicator("Q-Z"), (tabInfo = new TabInfo("Tab3", ArrayListFragment.class, args)));
        mapTabInfo.put(tabInfo.tag, tabInfo);

        mTabHost.setOnTabChangedListener(this);
    }

    private static void AddTab(MainActivity activity, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        tabSpec.setContent(activity.new TabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    public void onTabChanged(String tag) {
        clearSearch();

        int pos = this.mTabHost.getCurrentTab();
        this.mPager.setCurrentItem(pos);
    }


    public static class MyAdapter extends FragmentStatePagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return ArrayListFragment.newInstance(position);
        }

    }

    public static class ArrayListFragment extends Fragment {
        int mNum = 0;

        static ArrayListFragment newInstance(int num) {
            ArrayListFragment f = new ArrayListFragment();

            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);

            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum = getArguments() != null ? getArguments().getInt("num") : 0;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_layout, container, false);

            ListView lv = (ListView) v.findViewById(R.id.listUsers);

            if(mNum == 0)
                lv.setAdapter(mUserCursorAdapterA);
            else if(mNum == 1)
                lv.setAdapter(mUserCursorAdapterI);
            else if(mNum == 2)
                lv.setAdapter(mUserCursorAdapterQ);

            return v;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

    }


    private class LoadGitHubUsersListTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog;

        public LoadGitHubUsersListTask() {
            dialog = ProgressDialog.show(MainActivity.this, "Пожалуйста, подождите...", "Загружаем список пользователей", true, false);
        }

        private void loadGithubUsers() {
            Integer lastUser = 0;

            while (lastUser < 100) { // чтобы не нагружать особо github

                HtmlResponseWithHeaders json = getPage("https://api.github.com/users?client_id=4ed3ff6797ecb04c8e6b&client_secret=6d52fa85d89986e9171f84fc0af382b8016a75e5&since=" + lastUser);

                if (json == null)
                    break;
                if (avaibleRateLimit(json.getHeaders()) <= 0)
                    break;

                String strJSON = json.getHtml();
                if (strJSON != null) {
                    try {
                        JSONArray jArray = new JSONArray(strJSON);

                        for (int i = 0; i < jArray.length(); i++) {
                            try {
                                JSONObject oneObject = jArray.getJSONObject(i);
                                GithubUser user = new GithubUser();
                                lastUser = oneObject.getInt("id");
                                user.setUserId(lastUser);
                                user.setUserName(oneObject.getString("login"));
                                user.setAvatarUrl(oneObject.getString("avatar_url"));

                                HtmlResponseWithHeaders userJSON = getPage(oneObject.getString("url") + "?client_id=4ed3ff6797ecb04c8e6b&client_secret=6d52fa85d89986e9171f84fc0af382b8016a75e5");
                                if (userJSON == null)
                                    break;
                                if (avaibleRateLimit(userJSON.getHeaders()) <= 0)
                                    break;
                                String strUserJSON = userJSON.getHtml();

                                JSONObject userObject = new JSONObject(strUserJSON);

                                user.setUserFollowers(userObject.getInt("followers"));
                                user.setUserFollowing(userObject.getInt("following"));

                                mDb.addGithubUser(user);

                            } catch (JSONException e) {
                                // TODO
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            mDb = new DatabaseHelper(getApplicationContext());

            if(!(mDb.getUsersCount() > 0)) {
                if(Utils.isNetworkOnline(getApplicationContext())) {
                    loadGithubUsers();
                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainActivity.this, "No network connection avaible", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            mUserCursorAdapterA = new UserCursorAdapter(getApplicationContext(), mDb.getUsersByName(regexPart1, null), 0);
            mUserCursorAdapterI = new UserCursorAdapter(getApplicationContext(), mDb.getUsersByName(regexPart2, null), 0);
            mUserCursorAdapterQ = new UserCursorAdapter(getApplicationContext(), mDb.getUsersByName(regexPart3, null), 0);

            mAdapter = new MyAdapter(getSupportFragmentManager());
            mPager = (ViewPager)findViewById(R.id.viewpager);
            mPager.setAdapter(mAdapter);

            mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    clearSearch();
                    mTabHost.setCurrentTab(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        private Integer avaibleRateLimit(Map<String, List<String>> headers) {
            if(headers.containsKey("X-RateLimit-Remaining"))
                if(headers.get("X-RateLimit-Remaining").get(0) != null)
                    return Integer.valueOf(headers.get("X-RateLimit-Remaining").get(0));

            return 0;
        }

        private HtmlResponseWithHeaders getPage(String urlStr) {
            try {
                URL url = new URL(urlStr);
                URLConnection urlConnection = url.openConnection();
                urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36");

                urlConnection.setDoOutput(false);

                boolean okCode = false;

                for (Map.Entry<String, List<String>> entry : urlConnection.getHeaderFields().entrySet()) {
                    if(entry.getValue().contains("200 OK")) {
                        okCode = true;
                        break;
                    }
                }

                InputStream is;
                if(okCode)
                    is = urlConnection.getInputStream();
                else
                    return null;

                InputStreamReader isr = new InputStreamReader(is, Charset.forName("UTF-8").newDecoder()
                        .onMalformedInput(CodingErrorAction.REPORT)
                        .onUnmappableCharacter(CodingErrorAction.REPORT));

                int numCharsRead;
                char[] charArray = new char[1024];
                StringBuffer sb = new StringBuffer();
                while ((numCharsRead = isr.read(charArray)) > 0) {
                    sb.append(charArray, 0, numCharsRead);
                }

                return new HtmlResponseWithHeaders(sb.toString(), urlConnection.getHeaderFields());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        private class HtmlResponseWithHeaders {

            private String html;
            private Map<String, List<String>> headers;

            public HtmlResponseWithHeaders(String html, Map<String, List<String>> headers) {
                this.html = html;
                this.headers = headers;
            }

            public String getHtml() {
                return html;
            }

            public void setHtml(String html) {
                this.html = html;
            }

            public Map<String, List<String>> getHeaders() {
                return headers;
            }

            public void setHeaders(Map<String, List<String>> headers) {
                this.headers = headers;
            }

        }

    }

}