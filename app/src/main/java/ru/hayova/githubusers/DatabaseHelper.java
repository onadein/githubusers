package ru.hayova.githubusers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by hayova on 04.03.2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public final class GithubUsersDb {
        public GithubUsersDb() {}

        public abstract class UserEntry implements BaseColumns {
            public static final String TABLE_NAME = "users";
            public static final String COLUMN_NAME_ENTRY_ID = "user_id";
            public static final String COLUMN_NAME_USERNAME = "user_name";
            public static final String COLUMN_NAME_FOLLOWERS = "followers";
            public static final String COLUMN_NAME_FOLLOWING = "following";
            public static final String COLUMN_NAME_AVATARURL = "avatar_url";
        }
    }

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "githubusers.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + GithubUsersDb.UserEntry.TABLE_NAME + " (" +
                    GithubUsersDb.UserEntry._ID + " INTEGER PRIMARY KEY," +
                    GithubUsersDb.UserEntry.COLUMN_NAME_ENTRY_ID + INT_TYPE + COMMA_SEP +
                    GithubUsersDb.UserEntry.COLUMN_NAME_USERNAME + TEXT_TYPE + COMMA_SEP +
                    GithubUsersDb.UserEntry.COLUMN_NAME_FOLLOWERS + INT_TYPE + COMMA_SEP +
                    GithubUsersDb.UserEntry.COLUMN_NAME_FOLLOWING + INT_TYPE + COMMA_SEP +
                    GithubUsersDb.UserEntry.COLUMN_NAME_AVATARURL + TEXT_TYPE +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + GithubUsersDb.UserEntry.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addGithubUser(GithubUser githubUser) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(GithubUsersDb.UserEntry.COLUMN_NAME_ENTRY_ID, githubUser.getUserId());
        values.put(GithubUsersDb.UserEntry.COLUMN_NAME_USERNAME, githubUser.getUserName());
        values.put(GithubUsersDb.UserEntry.COLUMN_NAME_FOLLOWERS, githubUser.getUserFollowers());
        values.put(GithubUsersDb.UserEntry.COLUMN_NAME_FOLLOWING, githubUser.getUserFollowing());
        values.put(GithubUsersDb.UserEntry.COLUMN_NAME_AVATARURL, githubUser.getAvatarUrl());
        db.insert(GithubUsersDb.UserEntry.TABLE_NAME, null, values);
        db.close();
    }

    public Cursor getUsersByName(String regex, String userName) {
        String selectQuery = "SELECT  * FROM " + GithubUsersDb.UserEntry.TABLE_NAME + " WHERE " +
                GithubUsersDb.UserEntry.COLUMN_NAME_USERNAME + " REGEXP '" + regex + "'" +
                (userName != null && !userName.isEmpty() ? " AND " + GithubUsersDb.UserEntry.COLUMN_NAME_USERNAME + " LIKE " + "'%" + userName + "%' " : "")
                + " ORDER BY " + GithubUsersDb.UserEntry.COLUMN_NAME_USERNAME + " COLLATE NOCASE ASC";

        SQLiteDatabase db = this.getReadableDatabase();

        return db.rawQuery(selectQuery, null);
    }

    public int getUsersCount() {
        String countQuery = "SELECT  * FROM " + GithubUsersDb.UserEntry.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

}