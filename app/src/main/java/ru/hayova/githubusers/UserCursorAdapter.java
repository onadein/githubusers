package ru.hayova.githubusers;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by hayova on 04.03.2016.
 */
public class UserCursorAdapter extends CursorAdapter {

    LayoutInflater mInflater;
    ImageLoader mImageLoader;

    public UserCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageLoader = ImageLoader.getInstance();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.user_row, parent, false);
    }

    public class ViewHolder {
        TextView username;
        TextView userstats;
        ImageView userimage;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        //ViewHolder viewHolder;

        String usernameDb = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.GithubUsersDb.UserEntry.COLUMN_NAME_USERNAME));
        String avatarDb = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.GithubUsersDb.UserEntry.COLUMN_NAME_AVATARURL));
        int followers = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.GithubUsersDb.UserEntry.COLUMN_NAME_FOLLOWERS));
        int following = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.GithubUsersDb.UserEntry.COLUMN_NAME_FOLLOWING));

        //if(view == null) {
        //    view = mInflater.inflate(R.layout.user_row, null);
        //    viewHolder = new ViewHolder();

        TextView username = (TextView) view.findViewById(R.id.username);
        TextView userstats = (TextView) view.findViewById(R.id.userstats);
        ImageView userimage = (ImageView) view.findViewById(R.id.userimage);

        //    view.setTag(viewHolder);
        //} else
        //    viewHolder = (ViewHolder) view.getTag();

        username.setText(usernameDb);
        userstats.setText(followers + "/" + following);

        mImageLoader.displayImage(avatarDb, userimage);

    }
}
