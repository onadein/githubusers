package ru.hayova.githubusers;

/**
 * Created by hayova on 03.03.2016.
 */
public class GithubUser implements Comparable<GithubUser> {

    private Integer userId;
    private String userName;
    private Integer userFollowers;
    private Integer userFollowing;
    private String avatarUrl;

    public GithubUser(){};

    public GithubUser(Integer userId, String userName, Integer userFollowers, Integer userFollowing, String avatarUrl) {
        this.userId = userId;
        this.userName = userName;
        this.userFollowers = userFollowers;
        this.userFollowing = userFollowing;
        this.avatarUrl = avatarUrl;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserFollowers() {
        return userFollowers;
    }

    public void setUserFollowers(Integer userFollowers) {
        this.userFollowers = userFollowers;
    }

    public Integer getUserFollowing() {
        return userFollowing;
    }

    public void setUserFollowing(Integer userFollowing) {
        this.userFollowing = userFollowing;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public int compareTo(GithubUser another) {
        return this.userName.toLowerCase().compareTo(another.userName.toLowerCase());
    }
}
