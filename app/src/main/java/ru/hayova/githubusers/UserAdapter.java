package ru.hayova.githubusers;

import android.app.Activity;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by hayova on 03.03.2016.
 */
public class UserAdapter extends BaseAdapter implements Filterable {

    private Activity activity;

    private ArrayList<GithubUser> originalData = null;
    private ArrayList<GithubUser> filteredData = null;

    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;

    private ItemFilter filter = new ItemFilter();

    public UserAdapter(Activity activity, ArrayList<GithubUser> data) {
        this.activity = activity;
        originalData = data;
        filteredData = data;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageLoader.getInstance();
    }

    public int getCount() {
        return (filteredData != null ? filteredData.size() : 0);
    }

    public Object getItem(int position) {
        return (filteredData != null ? filteredData.get(position) : null);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.user_row, null);

        TextView username = (TextView)vi.findViewById(R.id.username);
        TextView userstats = (TextView)vi.findViewById(R.id.userstats);
        ImageView userimage = (ImageView)vi.findViewById(R.id.userimage);

        if(filteredData != null) {
            GithubUser user = filteredData.get(position);

            username.setText(user.getUserName());
            userstats.setText(user.getUserFollowers() + "/" + user.getUserFollowing());

            imageLoader.displayImage(user.getAvatarUrl(), userimage);
        }

        return vi;
    }

    public Filter getFilter() {
        return this.filter;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<GithubUser> list = originalData;

            int count = list.size();
            final ArrayList<GithubUser> nlist = new ArrayList<GithubUser>(count);

            GithubUser filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i);
                if (filterableString.getUserName().toLowerCase().contains(filterString)) {
                    nlist.add(filterableString);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<GithubUser>) results.values;
            notifyDataSetChanged();
        }

    }

}
