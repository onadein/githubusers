package ru.hayova.githubusers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by hayova on 09.03.2016.
 */
public class Utils {
    public static boolean isNetworkOnline(Context context) {
        boolean isConnected = false;
        try{
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            isConnected = activeNetwork.isConnectedOrConnecting();
        }catch(Exception e){
            return false;
        }
        return isConnected;
    }
}
